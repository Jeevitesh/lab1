import random
from search import linear_search
from search import binary_search
import matplotlib.pyplot as plt
import time

worst_array_linear = []
worst_array_binary = []
best_array_linear = []
best_array_binary = []
item_array = []

#initial items fed
items = 500000

#iterating for comparision
for i in range(15):
    #generating the random data to be searched
    data = random.sample(range(1000000000), items)
    data.sort()

    #binary search worst case:
    start = time.time()
    binary_search(data, data[0])
    end = time.time()
    worst_time_binary = end - start
    worst_array_binary.append(worst_time_binary)

    #linear search worst case
    start = time.time()
    linear_search(data, data[len(data) - 1])
    end = time.time()
    worst_time_linear = end - start
    worst_array_linear.append(worst_time_linear)

    #binary search best case
    start = time.time()
    binary_search(data, data[(len(data) - 1) // 2])
    end = time.time()
    best_time_binary = end - start
    best_array_binary.append(best_time_binary)

    #linear search best case
    start = time.time()
    linear_search(data, data[0])
    end = time.time()
    best_time_linear = end - start
    best_array_linear.append(best_time_linear)

    #iteration for next step
    item_array.append(items)
    items += 2000000

#plotting the graph
worst_case = plt.figure(1)
plt.plot(item_array, worst_array_linear, label="linear_worst")
plt.plot(item_array, worst_array_binary, label="binary_worst")
plt.xlabel("items-in-list")
plt.ylabel("execution-time(seconds)")
plt.title("Linear VS Binary(Worst-Case)")

best_case = plt.figure(2)
plt.plot(item_array, best_array_linear, label="linear_best")
plt.plot(item_array, best_array_binary, label="binary_best")
plt.xlabel("items-in-list")
plt.ylabel("execution-time(seconds)")
plt.title("Linear VS Binary(Best-Case)")

plt.legend()
plt.show()

print("Linear times(worst-case): ",worst_array_linear)
print("Binary times(worst-case): ",worst_array_binary)
print("Linear times(best-case): ",best_array_linear)
print("Binary times(best-case): ",best_array_binary)
