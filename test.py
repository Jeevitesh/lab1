import random
from search import linear_search
from search import binary_search
import time

data = random.sample(range(1000000000), 42500000)
data.sort()

start = time.time()
binary_search(data, data[0])
end = time.time()
worst_time_binary = end - start
print(worst_time_binary)

start = time.time()
linear_search(data, data[len(data) - 1])
end = time.time()
worst_time_linear = end - start
print(worst_time_linear)
