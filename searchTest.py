import unittest
from search import linear_search
from search import binary_search

class TestSearch(unittest.TestCase):

    def test_search_linear(self):
        data = [1, 2, 3, 5, 6, 12, 7, 4, 8]
        self.assertEqual(linear_search(data, 6), 4)
        self.assertEqual(linear_search(data, 10),  -1)

    def test_searchChar(self):
        data = ['t', 'a', 'b', 'l', 'e']
        self.assertEqual(linear_search(data, 'a'), 1)
        self.assertEqual(binary_search(data, "l"), 3)

    def test_search_binary(self):
        data = [1, 2, 3, 4, 6, 7, 12, 54]  #pre-sorted data
        self.assertEqual(binary_search(data, 12), 6)
        self.assertEqual(binary_search(data, 8), -1)

    def test_binary_char(self):
        data = ['a', 'e', 'i', 'o', 'u']    #pre-sorted data
        self.assertEqual(binary_search(data, 'u'), 4)
        self.assertEqual(binary_search(data, 'z'), -1)

if __name__ == "__main__":
    unittest.main()