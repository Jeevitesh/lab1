#Linear Search
def linear_search(data, target):
    for i in range(len(data)):
        if data[i] == target:
            return i
    return -1

#Binary Search
def binary_search(data, target):
    low = 0
    high = len(data) - 1
    mid = 0

    while low <= high:
        mid = (low + high) // 2
        if data[mid] < target:
            low = mid + 1

        elif data[mid] > target:
            high = mid - 1
        
        else:
            return mid
    return -1



